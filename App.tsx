import { useCallback, useState } from 'react';
import { ScrollView, StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';

interface Todos {
	id: number;
	todo: string;
}

export default function App() {
	const [todos, setTodos] = useState<Todos[]>([]);
	const [input, setInput] = useState('');
	const [isEdit, setIsEdit] = useState(false);
	const [todoEdited, setTodoEdited] = useState<Todos | null>(null);

	const addTodo = useCallback(() => {
		setTodos([...todos, { id: Date.now(), todo: input }]);
		setInput('');
	}, [input]);

	const deleteTodo = useCallback(
		(id: number) => {
			const filterTodo = todos.filter(todo => todo.id !== id);
			setTodos(filterTodo);
		},
		[todos]
	);

	const cancelEdit = useCallback(() => {
		setInput('');
		setIsEdit(false);
		setTodoEdited(null);
	}, []);

	const updateTodo = useCallback(() => {
		const filterTodo = todos.filter(todo => todo.id !== todoEdited?.id);
		const copyTodo = { ...todoEdited };
		copyTodo.todo = input;
		setTodos([...filterTodo, copyTodo as Todos]);
		setInput('');
	}, [input, todos, todoEdited]);

	return (
		<ScrollView style={styles.layout}>
			<View style={{ marginVertical: 20 }}>
				<Text style={{ fontSize: 24, fontWeight: '600', textAlign: 'center' }}>ToDoList</Text>
			</View>

			<View>
				<TextInput value={input} onChangeText={setInput} style={{ borderWidth: 1, borderColor: 'gray', padding: 5 }} />
				<View style={{ flexDirection: 'row', marginTop: 10 }}>
					<TouchableOpacity
						onPress={() => (isEdit ? updateTodo() : addTodo())}
						style={{ backgroundColor: '#06b6d4', padding: 10, borderRadius: 5, alignSelf: 'flex-start', marginRight: 10 }}
					>
						<Text style={{ color: 'white' }}>{isEdit ? 'Edit Todo' : 'Add Todo'}</Text>
					</TouchableOpacity>
					{isEdit && (
						<TouchableOpacity onPress={cancelEdit} style={{ backgroundColor: '#ef4444', padding: 10, borderRadius: 5, alignSelf: 'flex-start' }}>
							<Text style={{ color: 'white' }}>Cancel Edit</Text>
						</TouchableOpacity>
					)}
				</View>
			</View>

			<View style={{ marginTop: 20 }}>
				{todos
					.sort((a, b) => a.id - b.id)
					.map(todo => (
						<View
							key={todo.id}
							style={{ marginBottom: 10, borderWidth: 1, borderColor: '#a1a1aa', borderRadius: 5, paddingHorizontal: 5, paddingVertical: 10 }}
						>
							<Text>{todo.todo}</Text>
							<View style={{ flexDirection: 'row', marginTop: 15 }}>
								<TouchableOpacity
									onPress={() => deleteTodo(todo.id)}
									style={{ backgroundColor: '#ef4444', padding: 5, borderRadius: 3, alignSelf: 'flex-start', marginRight: 10 }}
								>
									<Text style={{ color: 'white' }}>Delete</Text>
								</TouchableOpacity>
								<TouchableOpacity
									onPress={() => {
										setIsEdit(true);
										setInput(todo.todo);
										setTodoEdited(todo);
									}}
									style={{ backgroundColor: '#22c55e', padding: 5, borderRadius: 3, alignSelf: 'flex-start' }}
								>
									<Text style={{ color: 'white' }}>Edit</Text>
								</TouchableOpacity>
							</View>
						</View>
					))}
			</View>
		</ScrollView>
	);
}

const styles = StyleSheet.create({
	layout: {
		maxWidth: 1024,
		width: '90%',
		alignSelf: 'center',
	},
});
